### APP

Mon Petit Garage helps you to keep an eye on your bike parts.
How many kilometers I've made with this bike?
Is it the right moment to change my chain ?

### DATABASE

Run the docker with:
```bash
docker run -p5432:5432 --name monpetitgarage -e POSTGRES_PASSWORD="bike" -e POSTGRES_USER="monpetitgarage" -e POSTGRES_DB="monpetitgarage" -d postgres
```
Then run alembic
```bash
alembic upgrade head
```

### CONFIG

Config file is create with the thirdparty [confz](https://github.com/Zuehlke/ConfZ)
`config.yml` is your config file.


### Mockups
https://marvelapp.com/project/6400465/design/88586187
