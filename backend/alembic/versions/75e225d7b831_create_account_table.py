"""create account table

Revision ID: 75e225d7b831
Revises:
Create Date: 2022-07-18 22:43:54.831889

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "75e225d7b831"
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "account",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("name", sa.String(50), nullable=False),
        sa.Column("description", sa.Unicode(200)),
    )


def downgrade() -> None:
    op.drop_table("account")
