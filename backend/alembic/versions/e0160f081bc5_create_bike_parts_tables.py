"""Create bike parts tables

Revision ID: e0160f081bc5
Revises: fcc36667a3f6
Create Date: 2023-04-01 22:40:42.011062

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "e0160f081bc5"
down_revision = "fcc36667a3f6"
branch_labels = None
depends_on = None


RELATIONS = [
    ("drives", "drive_id"),
    ("chains", "chain_id"),
    ("derailleurs", "derailleur_id"),
    ("forks", "fork_id"),
    ("shocks", "shock_id"),
    ("tires_front", "tire_front_id"),
    ("tires_rear", "tire_rear_id"),
    ("wheels_front", "wheel_front_id"),
    ("wheels_rear", "wheel_rear_id"),
]


def upgrade() -> None:
    for column_name in [
        "chain_km",
        "chain_name",
        "derailleur_km",
        "derailleur_name",
        "drive_km",
        "drive_name",
        "tire_front_km",
        "tire_front_name",
        "tire_rear_km",
        "tire_rear_name",
        "wheel_front_km",
        "wheel_front_name",
        "wheel_rear_km",
        "wheel_rear_name",
    ]:
        op.drop_column("bikes", column_name)

    for table_name, fk in RELATIONS:
        op.create_table(
            table_name,
            sa.Column("id", sa.Integer(), autoincrement=True, nullable=False),
            sa.Column("name", sa.VARCHAR(), autoincrement=False, nullable=True),
            sa.Column(
                "kilometers",
                sa.Integer(),
                autoincrement=False,
                nullable=True,
                server_default="0",
            ),
            sa.PrimaryKeyConstraint("id", name=f"{table_name}_pkey"),
        )
        op.add_column(
            "bikes",
            sa.Column(
                fk,
                sa.Integer(),
                sa.ForeignKey(f"{table_name}.id", ondelete="CASCADE"),
                nullable=False,
            ),
        )


def downgrade() -> None:
    for table_name, fk in RELATIONS:
        op.drop_table(table_name)
        op.drop_column("bikes", fk)

    for column_name in [
        "chain_name",
        "derailleur_name",
        "drive_name",
        "tire_front_name",
        "tire_rear_name",
        "wheel_front_name",
        "wheel_rear_name",
    ]:
        op.add_column(
            "bikes",
            sa.Column(column_name, sa.VARCHAR(), autoincrement=False, nullable=True),
        )
    for column_name in {
        "chain_km",
        "derailleur_km",
        "drive_km",
        "tire_front_km",
        "tire_rear_km",
        "wheel_front_km",
        "wheel_rear_km",
    }:
        op.add_column(
            "bikes",
            sa.Column(
                column_name,
                sa.Integer(),
                autoincrement=False,
                nullable=True,
                server_default="0",
            ),
        )
