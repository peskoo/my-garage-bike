"""Added full details columns in bikes table

Revision ID: f83c31125e6b
Revises: 37b74ded63a1
Create Date: 2023-03-29 23:49:36.560063

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "f83c31125e6b"
down_revision = "37b74ded63a1"
branch_labels = None
depends_on = None


def upgrade() -> None:
    for column_name in [
        "chain_name",
        "derailleur_name",
        "drive_name",
        "tire_front_name",
        "tire_rear_name",
        "wheel_front_name",
        "wheel_rear_name",
    ]:
        op.add_column(
            "bikes",
            sa.Column(column_name, sa.VARCHAR(), autoincrement=False, nullable=True),
        )
    for column_name in {
        "chain_km",
        "derailleur_km",
        "drive_km",
        "tire_front_km",
        "tire_rear_km",
        "wheel_front_km",
        "wheel_rear_km",
    }:
        op.add_column(
            "bikes",
            sa.Column(
                column_name,
                sa.Integer(),
                autoincrement=False,
                nullable=True,
                server_default="0",
            ),
        )


def downgrade() -> None:
    for column_name in [
        "chain_km",
        "chain_name",
        "derailleur_km",
        "derailleur_name",
        "drive_km",
        "drive_name",
        "tire_front_km",
        "tire_front_name",
        "tire_rear_km",
        "tire_rear_name",
        "wheel_front_km",
        "wheel_front_name",
        "wheel_rear_km",
        "wheel_rear_name",
    ]:
        op.drop_column("bikes", column_name)
