"""Added kilometers column in bikes table

Revision ID: fcc36667a3f6
Revises: f83c31125e6b
Create Date: 2023-03-31 19:48:55.094742

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "fcc36667a3f6"
down_revision = "f83c31125e6b"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column(
        "bikes",
        sa.Column(
            "kilometers",
            sa.Integer(),
            autoincrement=False,
            nullable=True,
            server_default="0",
        ),
    )


def downgrade() -> None:
    op.drop_column("bikes", "kilometers")
