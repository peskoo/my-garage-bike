from pathlib import Path

from confz import ConfZ, ConfZFileSource

PROJECT_DIR = Path(__file__).parent.parent.resolve()


class DBConfig(ConfZ):
    sqlalchemy_uri: str


class Config(ConfZ):
    db: DBConfig

    CONFIG_SOURCES = ConfZFileSource(file=PROJECT_DIR / "config.yml")
