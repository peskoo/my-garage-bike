import logging
from typing import Optional

from fastapi import APIRouter, HTTPException
from pydantic import BaseModel

from monpetitgarage.api.kilometer import add, remove, reset
from monpetitgarage.db import session
from monpetitgarage.errors import BikeNotFound, UserNotFound

logger = logging.getLogger(__name__)
router = APIRouter()


class Datas(BaseModel):
    bike_name: str
    kilometers: Optional[int] = None


@router.put("/{username}/kilometers/add")
def add_kilometer(datas: Datas, username: str):
    logger.info("Welcome new bike...")

    # TODO: add authorized func.
    #    is_authorized(username)

    try:
        add(session, **datas.__dict__)
    except (BikeNotFound, UserNotFound) as e:
        raise HTTPException(status_code=404, detail=e.message)

    return f"Kilometers added : {datas.kilometers}"


@router.put("/{username}/kilometers/remove")
def remove_kilometer(datas: Datas, username: str):
    logger.info("Welcome new bike...")

    # TODO: add authorized func.
    #    is_authorized(username)

    try:
        remove(session, **datas.__dict__)
    except (BikeNotFound, UserNotFound) as e:
        raise HTTPException(status_code=404, detail=e.message)

    return f"Kilometers removed : {datas.kilometers}"


@router.put("/{username}/kilometers/reset")
def reset_kilometer(datas: Datas, username: str):
    logger.info("Welcome new bike...")

    # TODO: add authorized func.
    #    is_authorized(username)

    datas = datas.__dict__
    datas.pop("kilometers")

    try:
        reset(session, **datas)
    except (BikeNotFound, UserNotFound) as e:
        raise HTTPException(status_code=404, detail=e.message)

    return f"Kilometers removed : {datas.kilometers}"
