import logging
from typing import Optional

from fastapi import APIRouter, HTTPException
from pydantic import BaseModel

from monpetitgarage.api.bike import create, delete, get
from monpetitgarage.db import session
from monpetitgarage.errors import BikeNotFound, UserNotFound

logger = logging.getLogger(__name__)
router = APIRouter()


class Bike(BaseModel):
    brand: Optional[str]
    category: Optional[str]
    chain_km: Optional[int]
    chain_name: Optional[str]
    derailleur_km: Optional[int]
    derailleur_name: Optional[str]
    drive_km: Optional[int]
    drive_name: Optional[str]
    factory: Optional[int]
    fork_km: Optional[int]
    fork_name: Optional[str]
    name: Optional[str]
    purchased: Optional[int]
    shock_km: Optional[int]
    shock_name: Optional[str]
    tire_front_km: Optional[int]
    tire_front_name: Optional[str]
    tire_rear_km: Optional[int]
    tire_rear_name: Optional[str]
    wheel_front_km: Optional[int]
    wheel_front_name: Optional[str]
    wheel_rear_km: Optional[int]
    wheel_rear_name: Optional[str]


@router.put("/{username}/bikes/create")
def create_bike(bike: Bike, username: str):
    logger.info("Welcome new bike...")

    try:
        create(session, username=username, **bike.__dict__)
    except (BikeNotFound, UserNotFound) as e:
        raise HTTPException(status_code=404, detail=e.message)

    return f"bike : {bike.name} ; saved !"


@router.get("/{username}/bikes/{bike_id}")
def get_bike(bike_id: int):
    try:
        result = get(session, id=bike_id)
    except (BikeNotFound, UserNotFound) as e:
        raise HTTPException(status_code=404, detail=e.message)

    return result


@router.delete("/{username}/bikes/delete/{bike_id}")
def delete_bike(bike_id: int, username: str):
    try:
        delete(
            session,
            bike_id=bike_id,
            username=username,
        )
    except (BikeNotFound, UserNotFound) as e:
        raise HTTPException(status_code=404, detail=e.message)

    return f"bike {bike_id} deleted."
