from fastapi import APIRouter, HTTPException
from pydantic import BaseModel

from monpetitgarage.api.user import create, delete, get
from monpetitgarage.db import session
from monpetitgarage.errors import UserNotFound

router = APIRouter()


class UserToCreate(BaseModel):
    name: str
    password: str
    email: str


class UserToDelete(BaseModel):
    username: str
    password: str


@router.put("/users/create")
def create_user(user: UserToCreate):
    create(
        session,
        email=user.email,
        username=user.name,
        password=user.password,
    )
    return f"user : {user.name} ; saved !"


@router.get("/users/{user_id}")
def get_user(user_id: int):
    try:
        result = get(session, id=user_id)
    except UserNotFound as e:
        raise HTTPException(status_code=404, detail=e.message)

    return result


@router.delete("/users/delete")
def delete_user(user: UserToDelete):
    try:
        delete(
            session,
            username=user.username,
            password=user.password,
        )
    except UserNotFound as e:
        raise HTTPException(status_code=404, detail=e.message)

    return f"user {user.username} deleted."
