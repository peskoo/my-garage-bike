from dataclasses import dataclass, field

from sqlalchemy import Column, ForeignKey, Integer, String

from monpetitgarage.db import mapper_registry


@mapper_registry.mapped
@dataclass
class Bike:
    """Bike class."""

    __tablename__ = "bikes"
    __sa_dataclass_metadata_key__ = "sa"

    id: int = field(
        init=False,
        metadata={"sa": Column(Integer, autoincrement=True, primary_key=True)},
    )
    user_id: int = field(
        metadata={"sa": Column(Integer, ForeignKey("users.id", ondelete="CASCADE"))},
    )
    chain_id: int = field(metadata={"sa": Column(Integer, ForeignKey("chains.id"))})
    derailleur_id: int = field(
        metadata={
            "sa": Column(Integer, ForeignKey("derailleurs.id", ondelete="CASCADE"))
        }
    )
    drive_id: int = field(
        metadata={"sa": Column(Integer, ForeignKey("drives.id", ondelete="CASCADE"))}
    )
    fork_id: int = field(
        metadata={"sa": Column(Integer, ForeignKey("forks.id", ondelete="CASCADE"))}
    )
    shock_id: int = field(
        metadata={"sa": Column(Integer, ForeignKey("shocks.id", ondelete="CASCADE"))}
    )
    tire_front_id: int = field(
        metadata={
            "sa": Column(Integer, ForeignKey("tires_front.id", ondelete="CASCADE"))
        }
    )
    tire_rear_id: int = field(
        metadata={
            "sa": Column(Integer, ForeignKey("tires_rear.id", ondelete="CASCADE"))
        }
    )
    wheel_front_id: int = field(
        metadata={
            "sa": Column(Integer, ForeignKey("wheels_front.id", ondelete="CASCADE"))
        }
    )
    wheel_rear_id: int = field(
        metadata={
            "sa": Column(Integer, ForeignKey("wheels_rear.id", ondelete="CASCADE"))
        }
    )
    brand: str = field(default=None, metadata={"sa": Column(String(50))})
    category: str = field(default=None, metadata={"sa": Column(String(50))})
    factory: int = field(default=None, metadata={"sa": Column(Integer)})
    kilometers: int = field(default=0, metadata={"sa": Column(Integer)})
    name: str = field(default=None, metadata={"sa": Column(String(50))})
    purchased: int = field(default=None, metadata={"sa": Column(Integer)})


@mapper_registry.mapped
@dataclass
class Chain:
    """The chain."""

    __tablename__ = "chains"
    __sa_dataclass_metadata_key__ = "sa"

    id: int = field(
        init=False,
        metadata={"sa": Column(Integer, autoincrement=True, primary_key=True)},
    )
    kilometers: int = field(default=0, metadata={"sa": Column(Integer)})
    name: str = field(default=None, metadata={"sa": Column(String(50))})


@mapper_registry.mapped
@dataclass
class Derailleur:
    """The derailleur."""

    __tablename__ = "derailleurs"
    __sa_dataclass_metadata_key__ = "sa"

    id: int = field(
        init=False,
        metadata={"sa": Column(Integer, autoincrement=True, primary_key=True)},
    )
    kilometers: int = field(default=0, metadata={"sa": Column(Integer)})
    name: str = field(default=None, metadata={"sa": Column(String(50))})


@mapper_registry.mapped
@dataclass
class Drive:
    """Pedalier."""

    __tablename__ = "drives"
    __sa_dataclass_metadata_key__ = "sa"

    id: int = field(
        init=False,
        metadata={"sa": Column(Integer, autoincrement=True, primary_key=True)},
    )
    kilometers: int = field(default=0, metadata={"sa": Column(Integer)})
    name: str = field(default=None, metadata={"sa": Column(String(50))})


@mapper_registry.mapped
@dataclass
class Fork:
    """Fork (front)."""

    __tablename__ = "forks"
    __sa_dataclass_metadata_key__ = "sa"

    id: int = field(
        init=False,
        metadata={"sa": Column(Integer, autoincrement=True, primary_key=True)},
    )
    kilometers: int = field(default=0, metadata={"sa": Column(Integer)})
    name: str = field(default=None, metadata={"sa": Column(String(50))})


@mapper_registry.mapped
@dataclass
class Shock:
    """Shock (rear)."""

    __tablename__ = "shocks"
    __sa_dataclass_metadata_key__ = "sa"

    id: int = field(
        init=False,
        metadata={"sa": Column(Integer, autoincrement=True, primary_key=True)},
    )
    kilometers: int = field(default=0, metadata={"sa": Column(Integer)})
    name: str = field(default=None, metadata={"sa": Column(String(50))})


@mapper_registry.mapped
@dataclass
class TireFront:
    """The front tire."""

    __tablename__ = "tires_front"
    __sa_dataclass_metadata_key__ = "sa"

    id: int = field(
        init=False,
        metadata={"sa": Column(Integer, autoincrement=True, primary_key=True)},
    )
    kilometers: int = field(default=0, metadata={"sa": Column(Integer)})
    name: str = field(default=None, metadata={"sa": Column(String(50))})


@mapper_registry.mapped
@dataclass
class TireRear:
    """The rear tire."""

    __tablename__ = "tires_rear"
    __sa_dataclass_metadata_key__ = "sa"

    id: int = field(
        init=False,
        metadata={"sa": Column(Integer, autoincrement=True, primary_key=True)},
    )
    kilometers: int = field(default=0, metadata={"sa": Column(Integer)})
    name: str = field(default=None, metadata={"sa": Column(String(50))})


@mapper_registry.mapped
@dataclass
class WheelFront:
    """The front wheel."""

    __tablename__ = "wheels_front"
    __sa_dataclass_metadata_key__ = "sa"

    id: int = field(
        init=False,
        metadata={"sa": Column(Integer, autoincrement=True, primary_key=True)},
    )
    kilometers: int = field(default=0, metadata={"sa": Column(Integer)})
    name: str = field(default=None, metadata={"sa": Column(String(50))})


@mapper_registry.mapped
@dataclass
class WheelRear:
    """The rear wheel."""

    __tablename__ = "wheels_rear"
    __sa_dataclass_metadata_key__ = "sa"

    id: int = field(
        init=False,
        metadata={"sa": Column(Integer, autoincrement=True, primary_key=True)},
    )
    kilometers: int = field(default=0, metadata={"sa": Column(Integer)})
    name: str = field(default=None, metadata={"sa": Column(String(50))})
