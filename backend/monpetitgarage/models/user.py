from dataclasses import dataclass, field

from sqlalchemy import Column, Integer, String

from monpetitgarage.db import mapper_registry


@mapper_registry.mapped
@dataclass
class User:
    """User class."""

    __tablename__ = "users"
    __sa_dataclass_metadata_key__ = "sa"

    id: int = field(
        init=False,
        metadata={"sa": Column(Integer, autoincrement=True, primary_key=True)},
    )
    email: str = field(metadata={"sa": Column(String(50))})
    username: str = field(metadata={"sa": Column(String(50))})
    password: str = field(metadata={"sa": Column(String(50))})
