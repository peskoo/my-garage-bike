from sqlalchemy import create_engine
from sqlalchemy.orm import registry, sessionmaker

from monpetitgarage.config import Config


engine = create_engine(Config().db.sqlalchemy_uri, future=True)
mapper_registry = registry()
_session = sessionmaker(bind=engine)

session = _session()
