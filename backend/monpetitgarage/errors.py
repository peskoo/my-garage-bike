class Base(Exception):
    """Base class for exceptions in this module."""

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message


class BikeNotFound(Base):
    """Exception raised if the bike does not exist in database.

    Args:
        message: explanation of the error
    """

    def __init__(self, message):
        super().__init__(message=message)


class InvalidValueParameter(Base):
    """Exception raised if the given parameter is invalid.

    Args:
        message: explanation of the error
    """

    def __init__(self, message):
        super().__init__(message=message)


class UserNotFound(Base):
    """Exception raised if the user does not exist in database.

    Args:
        message: explanation of the error
    """

    def __init__(self, message):
        super().__init__(message=message)
