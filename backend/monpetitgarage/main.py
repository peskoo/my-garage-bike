import logging

import uvicorn
from fastapi import FastAPI

from monpetitgarage.routes import bike, kilometer, user

app = FastAPI()
app.include_router(bike.router)
app.include_router(kilometer.router)
app.include_router(user.router)

logging.basicConfig(
    encoding="utf-8",
    level=logging.INFO,
)
logger = logging.getLogger(__name__)


@app.get("/")
def root():
    print("hello")


def main():
    logger.info("Opening uvicorn...")
    uvicorn.run(
        "monpetitgarage.main:app",
        host="0.0.0.0",
        port=8000,
        log_level="info",
        reload=True,
    )


if __name__ == "__main__":
    main()
