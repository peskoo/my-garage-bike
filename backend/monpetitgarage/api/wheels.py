import logging
from typing import Optional

from sqlalchemy.orm import Session

from monpetitgarage.models.bike import WheelFront, WheelRear

logger = logging.getLogger(__name__)


def create_front(
    session: Session,
    name: Optional[str] = "default",
    kilometers: Optional[int] = 0,
) -> WheelFront:
    """Create a new front wheel into database link to a bike.

    Args:
        session: SQL Alchemy connection.
        name: Name of your front wheel.
        kilometers: Number of kilometers.

    Return:
         WheelFront object
    """
    logger.info("Creating new front wheel...")
    result = WheelFront(name=name, kilometers=kilometers)

    session.add(result)
    session.commit()

    logger.info("Front wheel created.")
    return result


def create_rear(
    session: Session,
    name: Optional[str] = "default",
    kilometers: Optional[int] = 0,
) -> WheelRear:
    """Create a new rear wheel into database link to a bike.

    Args:
        session: SQL Alchemy connection.
        name: Name of your rear wheel.
        kilometers: Number of kilometers.

    Return:
         WheelRear object
    """
    logger.info("Creating new rear wheel...")
    result = WheelRear(name=name, kilometers=kilometers)

    session.add(result)
    session.commit()

    logger.info("Rear wheel created.")
    return result
