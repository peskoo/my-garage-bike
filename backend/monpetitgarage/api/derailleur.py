import logging
from typing import Optional

from sqlalchemy.orm import Session

from monpetitgarage.models.bike import Derailleur

logger = logging.getLogger(__name__)


def create(
    session: Session,
    name: Optional[str] = "default",
    kilometers: Optional[int] = 0,
) -> Derailleur:
    """Create a new derailleur into database link to a bike.

    Args:
        session: SQL Alchemy connection.
        name: Name of your derailleur.
        kilometers: Number of kilometers.

    Return:
         Derailleur object
    """
    logger.info("Creating new derailleur...")
    result = Derailleur(name=name, kilometers=kilometers)

    session.add(result)
    session.commit()

    logger.info("Derailleur created.")
    return result
