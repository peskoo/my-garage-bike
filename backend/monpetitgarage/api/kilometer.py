import logging
from typing import Optional

from sqlalchemy.orm import Session

from monpetitgarage.api.bike import get
from monpetitgarage.models.bike import Bike

logger = logging.getLogger(__name__)


def refresh_kilometers(
    bike: Bike,
    operation: str,
    kilometers: Optional[int],
) -> None:
    if operation == "addition":
        bike.kilometers += kilometers
    if operation == "soustraction":
        bike.kilometers -= kilometers
    if operation == "reset":
        bike.kilometers = 0


def add(
    session: Session,
    bike_name: str,
    kilometers: int,
) -> dict[str, int]:
    """Add kilometers to bike parts.

    Args:
        bike_name: The name of the bike you want to add kilometers on.
        kilometers: The number of kilometers to add.

    Returns:
        A dict with all impacted parts.
    """
    logger.info("Add kilometers...")

    bike = get(session, name=bike_name)
    refresh_kilometers(
        bike=bike,
        kilometers=kilometers,
        operation="addition",
    )

    session.commit()

    logger.info("Kilometers added.")
    return {bike.name: f"{bike.kilometers} km"}


def remove(
    session: Session,
    bike_name: str,
    kilometers: int,
) -> dict[str, int]:
    logger.info("Remove kilometers...")

    bike = get(session, name=bike_name)
    refresh_kilometers(
        bike=bike,
        kilometers=kilometers,
        operation="soustraction",
    )

    session.commit()
    logger.info("Kilometers removed.")

    return {bike.name: f"{bike.kilometers} km"}


def reset(
    session: Session,
    bike_name: str,
) -> dict[str, int]:
    logger.info("Reset kilometers...")

    bike = get(session, name=bike_name)
    refresh_kilometers(
        bike=bike,
        kilometers=None,
        operation="reset",
    )

    session.commit()
    logger.info("Kilometers reseted.")

    return {bike.name: f"{bike.kilometers} km"}
