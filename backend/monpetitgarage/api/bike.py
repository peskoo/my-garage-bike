import logging
from typing import Union

from sqlalchemy import select
from sqlalchemy.orm import Session

import monpetitgarage.api.user as user_api
from monpetitgarage.api import (
    chain,
    derailleur,
    drive,
    fork,
    shock,
    tires,
    wheels,
)
from monpetitgarage.errors import BikeNotFound
from monpetitgarage.models.bike import Bike

logger = logging.getLogger(__name__)


def create(session: Session, name: str, username: str, **details) -> Bike:
    """Create a new bike into database.

    Args:
        session: SQL Alchemy connection.
        name: Name of your bike.
        username: Owner of the new bike.
        **details:
            brand: brand's bike.
            category: Which bike is it [vtt, road, travel, gravel, tourism]
            factory: Official year of the bike from factory
            purchased: The year where the user purchased the bike

    Return:
         Bike object
    """
    logger.info("Creating new bike...")
    account = user_api.get(session, username=username)

    # Creating resources

    chain_ = chain.create(session)
    derailleur_ = derailleur.create(session)
    drive_ = drive.create(session)
    fork_ = fork.create(session)
    shock_ = shock.create(session)
    tire_front = tires.create_front(session)
    tire_rear = tires.create_rear(session)
    wheel_front = wheels.create_front(session)
    wheel_rear = wheels.create_rear(session)

    new_bike = Bike(
        name=name,
        user_id=account.id,
        chain_id=chain_.id,
        derailleur_id=derailleur_.id,
        drive_id=drive_.id,
        fork_id=fork_.id,
        shock_id=shock_.id,
        tire_front_id=tire_front.id,
        tire_rear_id=tire_rear.id,
        wheel_front_id=wheel_front.id,
        wheel_rear_id=wheel_rear.id,
        **details,
    )

    session.add(new_bike)
    session.commit()
    logger.info("Done.")

    return new_bike


def get(
    session: Session,
    **filters,
) -> Union[Bike, list[Bike]]:
    """Retrieve all the bikes you're looking for.

    Args:
        session: SQL Alchemy connection.
        **filters:
            The same as **details for create() func.

    Return:
        Bike obj or list of Bike objects
    """
    logger.info("Getting bikes...")
    statement = select(Bike).filter_by(**filters)
    result = session.execute(statement).scalars().all()

    if len(result) == 0:
        raise BikeNotFound(message="We do not find any bikes.")

    return result[0] if len(result) == 1 else result


def delete(session: Session, bike_id: int, username: str) -> None:
    """Delete a bike.

    Args:
        bike_id: id of the bike you want to delete

    Returns:
        None
    """
    logger.info("Deleting bike...")
    user = user_api.get(session, username=username)
    obj = get(session, id=bike_id, user_id=user.id)

    session.delete(obj)
    session.commit()
    logger.info(f"Bike {bike_id} deleted.")
