import logging
from typing import Optional

from sqlalchemy.orm import Session

from monpetitgarage.models.bike import Shock

logger = logging.getLogger(__name__)


def create(
    session: Session,
    name: Optional[str] = "default",
    kilometers: Optional[int] = 0,
) -> Shock:
    """Create a new shock into database link to a bike.

    Args:
        session: SQL Alchemy connection.
        name: Name of your shock.
        kilometers: Number of kilometers.

    Return:
         Shock object
    """
    logger.info("Creating new shock...")
    result = Shock(name=name, kilometers=kilometers)

    session.add(result)
    session.commit()

    logger.info("Shock created.")
    return result
