import logging
from typing import Optional

from sqlalchemy.orm import Session

from monpetitgarage.models.bike import Drive

logger = logging.getLogger(__name__)


def create(
    session: Session,
    name: Optional[str] = "default",
    kilometers: Optional[int] = 0,
) -> Drive:
    """Create a new drive into database link to a bike.

    Args:
        session: SQL Alchemy connection.
        name: Name of your drive.
        kilometers: Number of kilometers.

    Return:
         Drive object
    """
    logger.info("Creating new drive...")
    result = Drive(name=name, kilometers=kilometers)

    session.add(result)
    session.commit()

    logger.info("Drive created.")
    return result
