import logging
from typing import Optional

from sqlalchemy.orm import Session

from monpetitgarage.models.bike import Fork

logger = logging.getLogger(__name__)


def create(
    session: Session,
    name: Optional[str] = "default",
    kilometers: Optional[int] = 0,
) -> Fork:
    """Create a new fork into database link to a bike.

    Args:
        session: SQL Alchemy connection.
        name: Name of your fork.
        kilometers: Number of kilometers.

    Return:
         Fork object
    """
    logger.info("Creating new fork...")
    result = Fork(name=name, kilometers=kilometers)

    session.add(result)
    session.commit()

    logger.info("Fork created.")
    return result
