import logging
from typing import Optional

from sqlalchemy.orm import Session

from monpetitgarage.models.bike import TireFront, TireRear

logger = logging.getLogger(__name__)


def create_front(
    session: Session,
    name: Optional[str] = "default",
    kilometers: Optional[int] = 0,
) -> TireFront:
    """Create a new front tire into database link to a bike.

    Args:
        session: SQL Alchemy connection.
        name: Name of your front tire.
        kilometers: Number of kilometers.

    Return:
         TireFront object
    """
    logger.info("Creating new front tire...")
    result = TireFront(name=name, kilometers=kilometers)

    session.add(result)
    session.commit()

    logger.info("Front tire created.")
    return result


def create_rear(
    session: Session,
    name: Optional[str] = "default",
    kilometers: Optional[int] = 0,
) -> TireRear:
    """Create a new rear tire into database link to a bike.

    Args:
        session: SQL Alchemy connection.
        name: Name of your rear tire.
        kilometers: Number of kilometers.

    Return:
         TireRear object
    """
    logger.info("Creating new rear tire...")
    result = TireRear(name=name, kilometers=kilometers)

    session.add(result)
    session.commit()

    logger.info("Rear tire created.")
    return result
