import logging

from sqlalchemy import select
from sqlalchemy.orm import Session

from monpetitgarage.errors import UserNotFound
from monpetitgarage.models.user import User


logger = logging.getLogger(__name__)


def create(
    session: Session,
    email: str,
    username: str,
    password: str,
) -> User:
    """Create user account.

    Args:
        email: email for your account
        username: name for your account
        password: password for your account

    Returns:
        User

    Errors:
        Exception: For instance I catch everything,
        we'll later to be more precise.
    """
    logger.info("Creating new user...")
    try:
        new_user = User(
            email=email,
            password=password,
            username=username,
        )
        session.add(new_user)
        session.commit()
    except Exception as e:
        logger.error("Something wrong: %s", e)

    logger.info("User is created: %s", username)
    return new_user


def get(session: Session, username: str) -> User:
    """Retrieve all the users you're looking for.

    Args:
        session: SQL Alchemy connection.
        username: username.

    Returns:
        User object.

    Errors:
        UserNotFound: if user does not exist in database.
    """
    logger.info("Getting users...")
    statement = select(User).where(User.username == username)
    result = session.scalars(statement).first()

    if result is None:
        raise UserNotFound(
            message=f"We do not find any user with this username: {username}"
        )

    return result


def delete(session: Session, username: str, password: str) -> None:
    """Delete a user.

    Args:
        username: username of the user you want to delete
        password: password used by the user to delete.

    Returns:
        None

    Errors:
        Exception: For instance I catch everything,
        we'll later to be more precise.
    """
    user = get(session, username=username, password=password)

    try:
        session.delete(user)
        session.commit()
    except Exception as e:
        logger.error("Something wrong: %s", e)

    logger.info(f"User {username} deleted.")
