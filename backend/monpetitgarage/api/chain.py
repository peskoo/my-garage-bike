import logging
from typing import Optional

from sqlalchemy.orm import Session

from monpetitgarage.models.bike import Chain

logger = logging.getLogger(__name__)


def create(
    session: Session,
    name: Optional[str] = "default",
    kilometers: Optional[int] = 0,
) -> Chain:
    """Create a new chain into database link to a bike.

    Args:
        session: SQL Alchemy connection.
        name: Name of your chain.
        kilometers: Number of kilometers.

    Return:
         Chain object
    """
    logger.info("Creating new chain...")
    result = Chain(name=name, kilometers=kilometers)

    session.add(result)
    session.commit()

    logger.info("Chain created.")
    return result
