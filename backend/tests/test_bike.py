import pytest

import monpetitgarage.api.bike as bike_api
from monpetitgarage.errors import BikeNotFound


def test_create(bike_factory):
    obj = bike_factory()
    assert obj.brand == "Cannondale"


def test_get(session, bike_factory):
    bike = bike_factory()
    res = bike_api.get(session, name=bike.name)
    assert res.name == bike.name


def test_get_all(session, bike_factory):
    b1, b2, b3 = (bike_factory(name=x, brand="Canyon") for x in ["b1", "b2", "b3"])
    res = bike_api.get(session, brand="Canyon")
    assert len(res) == 3


def test_get_error(session):
    with pytest.raises(BikeNotFound) as e:
        bike_api.get(session, name="Luffy")
    assert e.value.message == "We do not find any bikes."


def test_delete(session, account, bike_factory):
    bike = bike_factory()
    assert bike_api.get(session, id=bike.id).name == bike.name

    bike_api.delete(session, bike_id=bike.id, username=account.username)
    with pytest.raises(BikeNotFound) as e:
        bike_api.get(session, id=bike.id)
    assert e.value.message == "We do not find any bikes."
