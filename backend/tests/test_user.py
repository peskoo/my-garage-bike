import pytest

import monpetitgarage.api.user as user_api
from monpetitgarage.errors import UserNotFound


def test_create(account):
    assert account.username == "test-account"
    assert account.password == "123456789"
    assert account.email == "test@account.com"


def test_get(session, account_factory):
    u1, u2 = (account_factory(username=x, email="hello@test.fr") for x in ["u1", "u2"])
    res_u1 = user_api.get(session, username=u1.username)
    assert res_u1.username == u1.username

    with pytest.raises(UserNotFound) as e:
        user_api.get(session, username="Luffy")
    assert e.value.message == ("We do not find any user with this username: Luffy")
