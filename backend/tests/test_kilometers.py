from monpetitgarage.api.kilometer import add, remove, reset


def test_add(session, bike_factory):
    bike = bike_factory()
    assert bike.kilometers == 0

    res = add(session, bike_name=bike.name, kilometers=15)
    assert res == {bike.name: "15 km"}


def test_remove(session, bike_factory):
    bike = bike_factory()

    # Add to all parts
    add(session, bike_name=bike.name, kilometers=15)

    # Then remove 5 kilometers to all parts.
    remove(
        session,
        bike_name=bike.name,
        kilometers=5,
    )
    assert bike.kilometers == 10


def test_reset_on_one_parts(session, bike_factory):
    bike = bike_factory()
    add(session, bike_name=bike.name, kilometers=15)

    # Reset all kilometers.
    reset(
        session,
        bike_name=bike.name,
    )

    assert bike.kilometers == 0
