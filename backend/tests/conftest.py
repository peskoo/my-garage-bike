import os
from typing import Optional

import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import close_all_sessions, sessionmaker

import monpetitgarage.api.bike as bike
import monpetitgarage.api.user as user
from monpetitgarage.config import Config
from monpetitgarage.errors import UserNotFound


SQLALCHEMY_DATABASE_URI = (
    os.getenv("SQLALCHEMY_DATABASE_URI") or Config().db.sqlalchemy_uri
)


@pytest.fixture()
def session():
    engine = create_engine(SQLALCHEMY_DATABASE_URI)
    Session = sessionmaker()
    connection = engine.connect()
    transaction = connection.begin()
    session = Session(bind=connection)

    yield session

    session.close()
    transaction.rollback()
    close_all_sessions()


@pytest.fixture()
def bike_factory(session, account):
    def __create(
        brand: str = "Cannondale",
        category: str = "VTT",
        factory: int = 2019,
        name: str = "Jekyll",
        purchased: int = 2020,
        username: str = account.username,
    ):
        return bike.create(
            session,
            brand=brand,
            category=category,
            factory=factory,
            name=name,
            username=username or account.username,
            purchased=purchased,
        )

    return __create


@pytest.fixture()
def account(session):
    try:
        account = user.get(session, username="test-account")
    except UserNotFound:
        account = user.create(
            session,
            username="test-account",
            password="123456789",
            email="test@account.com",
        )
    else:
        raise Exception(message="Something wrong during conftest creating account.")
    return account


@pytest.fixture()
def account_factory(session):
    def __create(
        username="test-account-factory",
        password="factory",
        email="test-factory@account.com",
    ):
        return user.create(
            session,
            username=username,
            password=password,
            email=email,
        )

    return __create
