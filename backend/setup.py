from setuptools import find_packages, setup


INSTALL_REQUIRES = [
    "alembic==1.7.7",
    "confz==1.6.1",
    "fastapi==0.79.0",
    "SQLalchemy==1.4.39",
    "psycopg2-binary==2.9.3",
    "uvicorn[standard]==0.18.2",
]

VERSION = "0.0.0a"


setup(
    name="monpetitgarage",
    version=VERSION,
    packages=find_packages(),
    url="https://lasalledutemps.fr",
    author="Pesko",
    author_email="pesko@lasalledutemps.fr",
    description="Keep an eye on your bike parts",
    install_requires=INSTALL_REQUIRES,
    entry_points={
        "console_scripts": [
            "monpetitgarage-server=monpetitgarage.main:main",
        ]
    },
    extras_require={
        "test": [
            "flake8>=4.0.1",
            "mypy>=0.971",
            "pre-commit>=2.20.0",
            "pytest>=7.1.2",
            "pytest-cov>=3.0.0",
        ]
    },
)
